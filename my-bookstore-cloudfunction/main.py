import pandas as pd
import json
from google.cloud import storage
from google.cloud import datastore

datastore_client = datastore.Client() # creating a datastore client first
storage_client = storage.Client() # for reading from the CSV file

def handle_book(request):
    if request.method == 'POST': 
        # Get the bucket name and file name from the request
        bucket = request.json.get('bucket') # we are passing the bucket name from cloud scheduler / curl command
        blob = request.json.get('file') # we are passing the file name from cloud scheduler /curl command
        
        if not bucket_name or not file_name:
            return {"error": "Missing bucket or file name in the request"}, 400
                        
        csv_data = blob.download_as_string().decode('utf-8') # Download the file
        
        df = pd.read_csv(csv_data) # Use Pandas to read the CSV data
        
        books_data = df.to_dict(orient='records') # Convert the DataFrame to a list of dictionaries
        
        for book_data in books_data:
            entity = datastore.Entity(key=datastore_client.key('Book')) # Save each book to Datastore
            entity.update(book_data)
            datastore_client.put(entity)
        
        return {"success": "Books added successfully"}, 200
    elif request.method == 'GET':
        book_name = request.json.get('name')
        if not book_name:
            return {"error": "Missing 'name' parameter in the request"}, 400
        
        # Query Datastore for books with matching name
        query = datastore_client.query(kind='Book')
        query.add_filter('book_name', '>=', book_name)
        query.add_filter('book_name', '<', book_name + u'\ufffd')  # This handles the wildcard search
        books = list(query.fetch())
        
        return books
    else:
        return {"error": "Unsupported method"}, 405
